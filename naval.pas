program Navale;			// ressources : tableau 10 sur 10, bâteau, torpille, destruction des bateau, retour à une cellule vide//

uses crt;

type coord=record
	 x:integer;
	 y:integer;
end;

{type bateau=record
	 x:integer;
	 y:integer;
	 vivant:boolean;
end;}

const maxlg=11;
	  maxla=11;
	  posixl=5;
	  posiyl=5;

var carte : array [1..maxlg, 1..maxla] of integer;
	player1, player2:char;
	b:coord;

procedure init; //ressources: //
var a, j, i, b:integer;

begin	
	for i:=1 to  maxlg do
		for j:=1 to maxla do
		carte[i,j]:=0;
		carte[posixl, posiyl]:=1;
		for i:=1 to 10 do
		begin
			a:=random(maxlg)+1;
			b:=random(maxla)+1;
			while carte[a,b]<>0 do
		begin
			a:=random(maxlg)+1;
			b:=random(maxla)+1;
		end;
		carte[a,b]:=i;
		end;
		{carte[posixl, posiyl]:=0;
		b.x:=posixl;
		b.y:=posiyl;}
end;

function i2o(x, y:integer):string;
var res:string;
begin
        begin
	case x of
	1: res:=' ';
	2: res :='1';
	3: res :='2';
	4: res :='3';
	5: res :='4';
	6: res :='5';
	7: res :='6';
	8: res :='7';
	9: res :='8';
	10: res :='9';
	11: res :='10';
	end;

        begin
	case y of
	1: res:='';
	2: res:='A';
	3: res:='B';
	4: res:='C';
	5: res:='D';
	6: res:='E';
	7: res:='F';
	8: res:='G';
	9: res:='H';
	10: res:='I';
	11: res:='J';
	end;

	i2o:=res;
end;

procedure affiche;
var i,j:integer;
begin
	for i:=1 to maxlg do
	begin
		for j:=1 to maxla do
			write(i2o(carte[i,j]));
			writeln;
	end;
end;


BEGIN
	clrscr;
	cursoroff;
	init;
	affiche;
	readln;
END.





